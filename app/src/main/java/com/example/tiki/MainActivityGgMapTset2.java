package com.example.tiki;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tiki.R;
import com.example.tiki.databinding.MainGgMapBinding;
import com.example.tiki.roomdatabase.ggMapDatabase;
import com.example.tiki.roomdatabase.ggMapEntity;
import com.example.tiki.viewmodule.GgMapViewModule;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivityGgMapTset2 extends AppCompatActivity{
    private ggMapAdapper mapAdapper;
    private boolean isShow;
    private boolean isDele;
    private boolean isReview;
    private List<ggMapEntity> ml ;
    private  Date dateStart = null;
    private Date dateEnd=null;

    //mvvm
    private MainGgMapBinding binding;
    private GgMapViewModule viewModule;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main_gg_map);
        binding= DataBindingUtil.setContentView(this, R.layout.main_gg_map);
        inits();
    }

    private void inits() {
        LinearLayoutManager manager =new LinearLayoutManager(this);
        //manager.setReverseLayout(true);
        binding.lDataLoca.setLayoutManager(manager);
        RecyclerView.ItemDecoration item= new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        binding.lDataLoca.addItemDecoration(item);
        mapAdapper=new ggMapAdapper();
        viewModule=new ViewModelProvider(this).get(GgMapViewModule.class);
        //mapAdapper.setLggMap();
        //lv.setAdapter(mapAdapper);
        ml=new ArrayList<>();
        //ll=new ArrayAdapter<ggMapEntity>(this,android.R.layout.simple_list_item_1, arrLocal);
        ml= ggMapDatabase.getInstance(this).GgMapDAO().getListGgMap();
        if(ml!=null && !ml.isEmpty()){
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());
            String daStart=ml.get(0).get_time();
            try {
                dateStart=df.parse(daStart);
                Calendar calendar= Calendar.getInstance();
                calendar.setTime(dateStart);
                calendar.add(Calendar.DATE,1);
                dateEnd=df.parse(df.format(calendar.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        loadData(ml);
        getFragment();
        viewModule.getmGgMap().observe(this, new Observer<ggMapEntity>() {
            @Override
            public void onChanged(ggMapEntity ggMapEntity) {
                SaveLocation(ggMapEntity);
            }
        });
        binding.btnShowData.setOnClickListener(v->{showDta();});
        binding.btnDeleteAllData.setOnClickListener(v->{
            deleAllData();
        });
        binding.btnDrawGgMap.setOnClickListener(v->{drawGgMap();});
    }

    private void deleAllData() {
        if(isShow){
            new AlertDialog.Builder(this)
                    .setTitle("Cảnh báo!")
                    .setMessage("Bạn có chắc chắn muốn xóa không?")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ggMapDatabase.getInstance(MainActivityGgMapTset2.this).GgMapDAO().deleteAllGgMap();
                            ml.clear();
                            ml=null;
                            isDele=true;
                            loadData(ml);
                        }
                    })
                    .setNegativeButton("Cancel",null)
                    .show();
        }
        else{
            Toast.makeText(this, "Hãy chọn Show Data", Toast.LENGTH_SHORT).show();
        }
    }
    private void drawGgMap(){
        if(ml==null || ml.isEmpty()){
            Toast.makeText(this, "Chưa có dữ liệu?", Toast.LENGTH_SHORT).show();
        }
        else {
            if (!isReview) {
                binding.btnShowData.setVisibility(View.GONE);
                binding.btnDeleteAllData.setVisibility(View.GONE);
                binding.lDataLoca.setVisibility(View.GONE);
                Fragment fragment = new FragmentReviewGgMap();
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fram_GgmapTest2, fragment)
                        .commit();
                binding.btnDrawGgMap.setText("Back <-");
                isReview = true;
            } else {
                binding.btnShowData.setVisibility(View.VISIBLE);
                binding.btnDeleteAllData.setVisibility(View.VISIBLE);
                binding.btnDrawGgMap.setText("Draw GgMap");
                isReview = false;
                getFragment();
            }
        }

    }
    private void showDta() {
        Date currentTime = Calendar.getInstance().getTime();
        if(!isShow){
            binding.lDataLoca.setVisibility( View.VISIBLE);
            isShow=true;
            binding.btnShowData.setText("Hide Data");
        }
        else {
            binding.lDataLoca.setVisibility( View.GONE);
            binding.btnShowData.setText("Show Data");
            isShow=false;
        }

    }

    private void getFragment() {
        //Fragment fragment =new FragmentBlankGgMapTest2();
        Fragment fragment =new FragmentBlankGgMapTest2();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fram_GgmapTest2,fragment)
                .commit();
    }
    public int i=0;
//    @Override
//    public void SendLocation(ggMapEntity l) {
    private void SaveLocation(ggMapEntity l){
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());
        //arrLocal.add(df.format(currentTime));
        try {
            if(ml==null || ml.isEmpty()) {
                dateStart = df.parse(df.format(currentTime));
                Calendar calendar= Calendar.getInstance();
                calendar.setTime(dateStart);
                calendar.add(Calendar.DATE,1);
                dateEnd=df.parse(df.format(calendar.getTime()));
                //dateEnd=df.parse("2022/01/18 09:59:00");
            }
            Date local= df.parse(df.format(currentTime));
            Log.d("latitude --->:  ", ""+l.get_latitude());
            Log.d("longitude --->:  ", ""+l.get_longitude());
            Log.d("date start --->:  ", ""+df.format(dateStart));
            Log.d("date end --->:  ", ""+df.format(dateEnd));
            Log.d("date local --->:  ", ""+df.format(local));
            assert local != null;
            Log.d("check date local with date end --->:  ", ""+local.compareTo(dateEnd));
            //Toast.makeText(this, "so sánh "+ local.compareTo(dateEnd), Toast.LENGTH_SHORT).show();
            if( local.compareTo(dateEnd)!=1){
                l.set_time(df.format(local));
                //ggMapDatabase.getInstance(this).GgMapDAO().insertGgMap(l);
                if(isDele){
                    ml=new ArrayList<>();
                    isDele=false;
                }
                else{
                    ggMapDatabase.getInstance(this).GgMapDAO().insertGgMap(l);
                    ml.add(l);
                    Log.e("","Dữ liệu trong ngày lưu thành công!");
                }
                loadData(ml);
            }
            else
                Log.e("","Dữ liệu qua ngày!");
            //ml.add(l);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        //ml.add(l);
    }

    private void loadData(List<ggMapEntity> p) {
        mapAdapper.setLggMap(p);
        if(p!=null)
            binding.lDataLoca.scrollToPosition(p.size()-1);
        binding.lDataLoca.setAdapter(mapAdapper);
    }


    public List<ggMapEntity> getMl() {
        return ml;
    }
}