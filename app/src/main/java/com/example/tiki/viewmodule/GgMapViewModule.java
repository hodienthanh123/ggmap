package com.example.tiki.viewmodule;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.tiki.ISendLocation;
import com.example.tiki.roomdatabase.ggMapEntity;

public class GgMapViewModule extends ViewModel implements ISendLocation {
    private MutableLiveData<ggMapEntity> mGgMap;
    private Context context;
    private ggMapEntity gg;

    public GgMapViewModule() {
        super();
        mGgMap=new MutableLiveData<>();
    }
    private void  getInits(ggMapEntity gg){
        if(gg==null){
            return;
        }
        else {
            mGgMap.setValue(gg);
        }
    }
    @Override
    public void SendLocation(ggMapEntity g) {
        gg=g;
        getInits(gg);
    }

    public MutableLiveData<ggMapEntity> getmGgMap() {
        return mGgMap;
    }

    public void setmGgMap(MutableLiveData<ggMapEntity> mGgMap) {
        this.mGgMap = mGgMap;
    }
}
